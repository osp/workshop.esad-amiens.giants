Title: tool page template
Date: 2018/03/28
Author: martin
Status: hidden

<!--
available metadata:
Category: mapping (! category is meant to be single)
Tags: maps, command line, vector, dithering, (! tags are multiple)
-->

## Subtitle, tagline, whatever descibes best the tool


* Who is developing the tool?
Many different people on list below > https://inkscape.org/fr/credits/


* Is the tool under active development?
It seems not, but developer are working to improve it


* When did the development of the tool start? / How old is the software?


* Does the tool have a Wikipedia page?


* What is the tool made for?
Drawing, vectorial editing

* Who is the tool made for?

for graphic designers, artists or every person wich are interest in it, ab d all people with a computer

* Wat is the license of the tool?
GPL License / 
GNU GENERAL PUBLIC LICENSE


* Is it possible to contribute to the tool? How? How many people are contributing?


* What does the tool cost? Is it a one time payment or a subscription model?


* Are there tools alike? What makes this tool different?


* Does the tool have a Graphical Interface?


* Is the interface clear?


* Is the interface looking like other applications or is it custom made?


* Does the tool have a command line interface / can you use the tool from the commandline?


* Can you automate the tool? Does the tool have a scripting interface / API?


* What are the filetypes the tool can read?


* What are the filetypes the tool can generate / export?


* Is there an official documentation and if so where?


* For which operating systems is this tool available?

