Title: Electron
Date: 2018/03/27
Author: Gijs
Status: Published
Category: javascript framework

<!--
available metadata:
Category: mapping (! category is meant to be single)
Tags: maps, command line, vector, dithering, (! tags are multiple)
-->

## Develop desktop applications using javascript & HTML


* Who is developing the tool?

Github. The company started the project under the name 'Atom-shell' as part of their editor 'Atom'.

* Is the tool under active development?

Yes.

![](images/electron-C.png)

* When did the development of the tool start? / How old is the software?

The project was first published in 2013.

* Does the tool have a Wikipedia page?

Yes: https://en.wikipedia.org/wiki/Electron_(software_framework)

* What is the tool made for?

Developing desktop applications using webtechnologies. It allows to develop dektop applications using web tecnologies: HTML, Javascript, Node.

* Who is the tool made for?

Developers

* Wat is the license of the tool?

MIT-license

* Is it possible to contribute to the tool? How? How many people are contributing?

Yes. Electron has a GitHub page and does accept pul-requests.

* What does the tool cost? Is it a one time payment or a subscription model?

The Framework is available for free.

* Are there tools alike? What makes this tool different?

Currently there are two alternatives: nw.js and the brackets-shell. Although the latter is specifically developed for the brackets editor and is not meant / designed to be used for other applications.

* Does the tool have a Graphical Interface?

No, it is a programming framework.

* Is the interface clear?

* Is the interface looking like other applications or is it custom made?

* Does the tool have a command line interface / can you use the tool from the commandline?

* Can you automate the tool? Does the tool have a scripting interface / API?

* What are the filetypes the tool can read?

* What are the filetypes the tool can generate / export?

* Is there an official documentation and if so where?

* For which operating systems is this tool available?

Applications made with Electron can be run on Linux, Mac and Windows. It's one of the essential features of the tool.

To test Electron I wrapped the TTX-inspector developed for the ['Dirty Variables' workshop](https://gitlab.constantvzw.org/osp/workshop.dirty-variables/tree/master/inspector) into an application.

![](images/electron-A.png)

![](images/electron-B.png)

![](images/electron-D.png)