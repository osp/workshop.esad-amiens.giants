Title: Open orienteering
Date: 2018/03/28
Author: Carmiche


<!--
available metadata:
Category: mapping (! category is meant to be single)
Tags: maps, command line, vector, dithering, (! tags are multiple)
-->

## orienteering mapmaking program 

![](images/cartes_1.png)
![](images/cartes_2.png)
![](images/cartes_3.png)
![](images/cartes_4.png)


* Who is developing the tool?
 
Peter Curtis (2012-2013) 
Kai Pastor 
Thomas Schöps initiator of the projet  (2012-2014)

* Is the tool under active development?

The tool still being under active development.

* When did the development of the tool start? / How old is the software?

The development of the tool started in 2012 (first alpha release).



* Does the tool have a Wikipedia page?

No, it doesn’t have a wiki page. But, still under development. 


* What is the tool made for?

OpenOrienteering is a project developing a collection of tools 
which help with the creation of orienteering maps and the organization 
of orienteering events.

* Who is the tool made for?

For every person who does orienteering races or "intended for interested developers 
and adventurous users who like to try new stuff and give feedback." (Thomas Schöps)


* What is the license of the tool?

GNU General Public Licence GPL version3


* Is it possible to contribute to the tool? How? How many people are contributing?

- Yes, it is possible to contribute to the tool.
- 31 contributors. 


* What does the tool cost? Is it a one time payment or a subscription model?

The tool is completly free "I will never demand money for it" (Thomas Schöps). 


* Are there tools alike? What makes this tool different?

There aren't tools alike, the system of created symbols, the the possibility 
to edit the templates colors and symbols alreday made and  also to use georeferencing
that defines the relationship between map paper coordinates and real world coordinates

* Does the tool have a Graphical Interface?

The tool does have a Graphical Interface.

* Is the interface clear?

At the beginning, the interface isn't very clear 
but the tool offers to the users some tricks to understand.

* Is the interface looking like other applications or is it custom made?

The interface is custom made, isn't really looking like other applications. 

* Does the tool have a command line interface / can you use the tool from the commandline?

The tool doesn't have a command line interface.

* Can you automate the tool? Does the tool have a scripting interface / API?
Yes, the tool does have API.

* What are the filetypes the tool can read?

The tool can read:
CRT (Cross Reference Table) files provide rules for assigning and replacing symbols.
OSM files (openstreetmap vectordata)
DFX, PIX, OCD, OMAP (openorienteering map)

* What are the filetypes the tool can generate / export?

The tool can generate filetypes like CMYK PDF export (vector output),
raster image export : Overprinting simulation (raster output)

* Is there an official documentation and if so where?

There is an offical documentation of the tools which is in the website and also on github.
http://www.openorienteering.org/mapper-manual/pages

* For which operating systems is this tool available?

The tool is also available for android.


