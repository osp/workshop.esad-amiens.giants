Title: Leocad
Date: 2018/03/28
Author: Bruce


<!--
available metadata:
Category: mapping (! category is meant to be single)
Tags: maps, command line, vector, dithering, (! tags are multiple)
-->

## Lego constructions

![](images/BigLeocad.png)
![](images/Tool1.png)
![](images/STSPLeocad.png)
![](images/ToolsLeocad.png)
![](images/Stellarleo1.png)
![](images/Stellarleo2.png)
![](images/Leocadlog.png)
![](images/Leocadlog2.png)
![](images/Leocadlog3.png)




* Who is developing the tool?


* Is the tool under active development?

Yes.

* When did the development of the tool start? / How old is the software?

1998.

* Does the tool have a Wikipedia page?

No.

* What is the tool made for?

3D Lego constructions, and export it as images or videos. 

* Who is the tool made for?

Both adults and children.

* Wat is the license of the tool?

 GNU General Public. GPLV2. 

* Is it possible to contribute to the tool? How? How many people are contributing?

Yes, by creating or using a custom library. 

* What does the tool cost? Is it a one time payment or a subscription model?

Nothing.

* Are there tools alike? What makes this tool different?

LDRAW - It's a software where all contributors can build there own bricks to import it on Leocad. 

* Does the tool have a Graphical Interface?

Yes.

* Is the interface clear?

Yes, very easy to use, it's designed to allow new users creating models without spending so much time to understand it. 

* Is the interface looking like other applications or is it custom made?

It looks like other constructions softwares interfaces. 


* Does the tool have a command line interface / can you use the tool from the commandline?

No. 

* Can you automate the tool? Does the tool have a scripting interface / API?

Not sure. You can import your own library but maybe not change anything else. 

* What are the filetypes the tool can read?

LDR and MPD files. 

* What are the filetypes the tool can generate / export?

LDR and MPD files too, and 3d studio; Bricklink; collada; CSV; HTML; POV RAY; Wavefront. 

* Is there an official documentation and if so where?

Yes, on the website and github. 
www.leocad.org

* For which operating systems is this tool available?

Windows, Linux, MacOS. 
