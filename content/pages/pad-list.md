Title: PadList & important links
Date: 2018/03/30

* http://pads.osp.kitchen/p/on-the-shoulders-of-giants
* http://pads.osp.kitchen/p/on-the-shoulders-of-giants-planning
* http://pads.osp.kitchen/p/on-the-shoulders-of-giants-questionaire
* http://pads.osp.kitchen/p/on-the-shoulders-of-giants-tools
* http://pads.osp.kitchen/p/on-the-shoulders-of-giants-demo-pages
* http://pads.osp.kitchen/p/on-the-shoulders-of-giants-css

Git repository: https://gitlab.com/osp-kitchen/workshop_esad-amiens_giants
