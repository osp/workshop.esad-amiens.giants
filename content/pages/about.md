Title: about
Date: 2018/03/30

This micro site gathers the results of experiments carried out by students of ESAD Amiens during an OSP Workshop as part of the thematic week [On the shoulders of giants](http://giants.esad-amiens.fr/)

### Abstract :
Open Source Publishing (OSP) is a collective of designers. We reflect on the practice of graphic design by considering its tools. We began by trying alternative tools, we now tend to develop are own tools for our own specific needs and interests.

Through the freedoms and obligations ensured by F/LOSS licenses, a diverse landscape of tools has developed. Software big and small, famous and obscure, close and foreign fuel modern graphic design. We'll start the workshop by introducing some of the software recipies we've made in recent years and the tools we used. We invite you to explore these —and others, not necessarily digital— by using them, to understand what they do, by profiling them to understand why they were made and what they are made of.

Together we'll develop a method to record our findings and to produce an output. Using the unix paradigm of piping, where small specialised utilities are chained, we can imagine designing inputs and outputs in pipelines with which we could write a biography or map out the contemporary practice of graphic design.

---

![](https://cloud.osp.kitchen/s/WSOd0BWqab9Xsm7/download?path=%2F&files=pechakucha.07.png)

![](http://blog.osp.kitchen/images/uploads/spoon.jpg)

Context for the spoon image: http://blog.osp.kitchen/tools/spoon.html

![](https://cloud.osp.kitchen/s/WSOd0BWqab9Xsm7/download?path=%2F&files=pechakucha.17.png)

### Bio :
OSP makes graphic design using only free and open source software—pieces of software that invite their users to take part in their elaboration. Founded in 2006 in the context of Brussels art organisation Constant, the OSP caravan now comprises a group of individuals from different background and practices: typography, graphic design, cartography, programming, mathematics, writing, performance. Through a collaborative practice, they work on workshops, commissioned or self-initiated projects, searching to redefine their playground, digging for a more intimate relation with their tools. http://osp.kitchen

### The workshop :
