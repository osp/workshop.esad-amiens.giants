Title: On the Shoulders of Giants Tool list
Date: 2018/03/28

## tool list:

Pick one and mark that you are working on it's review

* Gimp (picture editing)
* Laidout (layout, picture)
* Inkscape (vector drawing tool)
* Scribus (Desktop plublishing)
* Potrace (vectorisation bitmap images)
* Autotrace (vectorisation tool)
* Contourtrace (extension of autotrace) http://www.openorienteering.org/apps/contourtrace/
* ghostscript (PDF manipulation / optimization)
* pdftk (PDF manipulation)
* Graphviz (Grapheditor)
* Fontforge (fonteditor)
* Birdfont (fonteditor)
* Chrome (Webbrowser)
* Firefox (Webbrowser)
* Krita (painting)
* Blender (3D modeling)
* Cirkuit https://wwwu.uni-klu.ac.at/magostin/cirkuit.html
* Dia https://wiki.gnome.org/Apps/Dia/
* Fyre http://fyre.navi.cx/
* Hugin http://hugin.sourceforge.net/
* Darktable (RAW image manipulation)
* Imagemagick (image manipulation, command line) (also: http://www.fmwconcepts.com/imagemagick/index.php )
* Karbon (Vector drawing tool) https://www.calligra.org/karbon/
* LeoCad (Lego modeling) https://www.leocad.org/
* LibreCAD (Computer Aided Design) http://librecad.org/cms/home.html
* OpenSCAD (scripted CAD) http://www.openscad.org/
* <strike>QCad (Computer Aided Design) https://qcad.org/en/</strike> — Done by Colm
* Open Orienteering (orienteering map maker) http://www.openorienteering.org/apps/mapper/
* Qgis (Geographic Information System manipulation) https://qgis.org/en/site/
* Pinta (image editing) https://pinta-project.com/pintaproject/pinta/
* Plater (postion models for 3d printing, it also has an auto layout mode) https://github.com/kliment/Printrun
* Mandelbulber (Mandelbrot image generation) https://sites.google.com/site/mandelbulber/
* Grommit-MPX (Annotations on screen, meant for use during presentations) https://github.com/bk138/gromit-mpx
* PlotDrop (graphs) http://plotdrop.sourceforge.net/
* Structure Synth (generating 3D structures by a design grammar) http://structuresynth.sourceforge.net/
* paper.js (Javascript library aimed at making tools, http://paperjs.org )
* d3.js (Javascript library for data visualisation https://d3js.org/ )
