Title: InkScApe
Date: 2018/03/28
Author: martin


<!--
available metadata:
Category: mapping (! category is meant to be single)
Tags: maps, command line, vector, dithering, (! tags are multiple)
-->


## VectoriaL EditingG 


![](images/mona.svg)


* Who is developing the tool?

Ted Gould, Bryce Harrington, Nathan Hurst, MenTaLguY
and many different people on list below  https://inkscape.org/fr/credits/


* Is the tool under active development?

It seems not, but developer are working to improve it


* When did the development of the tool start? / How old is the software?

It start in 2003 (a fork of Sodipodi)



* Does the tool have a Wikipedia page?

https://fr.wikipedia.org/wiki/Inkscape


* What is the tool made for?

Drawing, vectorial editing

* Who is the tool made for?

For graphic designers, artists or every person wich are interest in it, ab d all people with a computer

* Wat is the license of the tool?

GPL License / GNU GENERAL PUBLIC LICENSE


* Is it possible to contribute to the tool? How? How many people are contributing?

Yes, by writing Core Inkscape code, writing inkscape extensions, packaging inkscape, testing it, reporting bugs, translating inkscape software and website and promote it


* What does the tool cost? Is it a one time payment or a subscription model?

It cost nothing, to my knowledge


* Are there tools alike? What makes this tool different?

Yes it is like Adobe Illustrator, CorelDRAW


* Does the tool have a Graphical Interface?

yes


* Is the interface clear?

yes

* Is the interface looking like other applications or is it custom made?

It look like some "old" application coming back from past or future


* Does the tool have a command line interface / can you use the tool from the commandline?

yes, but mightbe not on mac


* Can you automate the tool? Does the tool have a scripting interface / API?

maybe but for now i don't know how

* What are the filetypes the tool can read?

PNG, SVG, JPEG, ...


* What are the filetypes the tool can generate / export?

SVG

* Is there an official documentation and if so where?

https://inkscape.org/fr/apprendre/

* For which operating systems is this tool available?

Windows, mac OS, Linux / GNU


![](images/2.1.svg)
![](images/mona.svg)
![](images/huilegrasse.svg-path19988-4294966476.png)
![](images/huilegrasse.svg)
![](images/dessin.svg)
![](images/dessin-4.svg)
![](images/dessin-3.svg)
![](images/dessin-2.svg)
![](images/dessin-1.svg)
![](images/34-copie.svg)
![](images/11.svg)
![](images/10.svg)
![](images/9.svg)
![](images/6.svg)
![](images/4.svg)
![](images/2.svg)
![](images/2.1.svg)
![](images/'.svg)
![](images/mona1.svg)
![](images/MO2.svg)
![](images/MO.svg)
