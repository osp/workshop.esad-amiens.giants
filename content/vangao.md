Title: QGIS
Date: 2018/03/27
Author: fei

* Who is developing the tool?
  Gary Sherman.
  
  
* Is the tool under active development?
  Yes
  
* What is the tool made for?
  For Desktop GIS (Geographic information system)
  
* Who is the tool made for?
  Designer, artist and geographer
  
* When did the development of the tool start? / How old is the software?
  Started development in 2002, 15 years old
  
* Does the tool have a Wikipedia page?
  Yes
  
* Wat is the license of the tool?
  It is free software so the license under the GPL
  
* Is it possible to contribute to the tool? How? How many people are contributing?
  
* What does the tool cost? Is it a one time payment or a subscription model?
  This is a free tool
  
* Are there tools alike? What makes this tool different?
  ArcGIS, MapGIS . ArcGIS and MapGIS are payment, QGIS is free.
  
* Does the tool have a Graphical Interface?
  Yes
  
* Is the interface clear?
  almost
  
* Is the interface looking like other applications or is it custom made?
  I'm not sure but i think this app is look like other apps
  
* Does the tool have a command line interface / can you use the tool from the commandline?
  
  
* Can you automate the tool? Does the tool have a scripting interface / API?
  
  
* What are the filetypes the tool can read?
  
  
* What are the filetypes the tool can generate / export?
 
 
* Is there an official documentation and if so where?
 
 
* For which operating systems is this tool available?
  Windows, Linux, Mac OS
  
![](images/Lyon.png)
![](images/Amiens.png)
![](images/Tibet.png)
![](images/Paris.png)