# On The Shoulders of Giants — OSP Worshop

## ESAD Amiens

### 2018/03/28 → 2018/03/30

Through the freedoms and obligations ensured by F/LOSS licenses, a diverse landscape of tools has developed. Software big and small, famous and obscure, close 
and foreign fuel 
moderngraphic design. We'll start the workshop by introducing some of the software recipies we've made in recent years and the tools we used. We invite you to explore these —and others, 
not necessarily digital— by using them, to understand what they do, by profiling them to understand why they were made and what they are made of.

Together we'll develop a method to record our findings and to produce an output. Using the unix paradigm of piping, where small specialised utilities are chained, we can imagine 
designing inputs and outputs in pipelines with which we could write a biography or map out the contemporary practice of graphic design. 

### Pads with content 
http://pads.osp.kitchen/p/on-the-shoulders-of-giants

### Software reviews
https://osp-kitchen.gitlab.io/workshop_esad-amiens_giants/
